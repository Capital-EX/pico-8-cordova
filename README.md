# Pico8 Cordova Android App
### Demo Cordova app which wraps a Pico8 HTLM5 exported game.


The App works by faking keyboard input through on screen elements.

This is an example fuction for faking the left arrow key.
```js
var left = function (e) { 
		var event = new KeyboardEvent(e,{ key: 'ArrowLeft', keyCode: 37})
        // Do to a bug in how webkit handles KeyboardEvents
        // the keyCode value is always set to zero and must
        // be manually overrided.
        // See http://stackoverflow.com/a/23812767
		Object.defineProperty(event, 'keyCode', {get:function(){return this.keyCodeVal;}}); 
		event.keyCodeVal = 37; 
		window.document.dispatchEvent(event);
}
```

Then attach the function to the `ontouchdown` and `ontouchend` of any html element.
```html
<button ontouchdown="left('keydown')" ontouchend="left'('keyup')"></button>
```

To run you must have [Cordova](https://cordova.apache.org/), the [Android SDK](https://developer.android.com/index.html), and [Java 7 or higher](http://www.oracle.com/technetwork/java/javase/downloads/index.html) installed.


**Note:  The game I made for this demo is _unfinished_. I just needed a game to test on.